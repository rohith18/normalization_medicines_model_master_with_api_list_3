import re


def checking_regular_expressions_for_s(s):
    patterns = [r"\d+mg", r"\d+ml", r"\d+gm", r"\d+mcg", r"\d+%", r"\d+g", r"\d+'s", r"\d+`s"]
    replaced_s = s.lower()

    for item in patterns:
        pattern = re.compile(item)
        matches = pattern.finditer(replaced_s)
        for match in matches:
            text = replaced_s[match.span()[0]: match.span()[1]+1]
            replaced_s = replaced_s.replace(text, "")

    return replaced_s


def normalize_terms_for_s(s):

    try:
        replaced_s = (" ".join(s.split())).lower()

        replacable_strings = {" tablet": " tab", " tablets": " tab", " ointment": " oint", " capsule": " cap", " capsules": " cap", " caps": " cap", " condoms": " condom", " diapers": " diaper", "syrup": "syp", " injection": " inj", " drops": " drop", " &": " and", ",": "", ".": "", "-": " ", " xxl": " size", " xl": " size"}
        for key, value in replacable_strings.items():
            replaced_s = replaced_s.replace(key, value)
            
        replacable_strings_2 = {" tabs": " tab", " caps": " cap", " xsize": " size", " xxxl": " size"}
        for key, value in replacable_strings_2.items():
            replaced_s = replaced_s.replace(key, value)

        if (("(" and ")") in replaced_s):
            replaced_s = replaced_s.replace(replaced_s[ replaced_s.find("(") : replaced_s.find(")") + 1], "")

        replaced_str_list = replaced_s.split()
        updated_replaced_str_list = []
        for item in replaced_str_list:
            if item.isdecimal():
                pass
            else:
                updated_replaced_str_list.append(item)

        replaced_s = " ".join(updated_replaced_str_list)
        replaced_s = " ".join(replaced_s.split())
    except AttributeError:
        pass

    return replaced_s


def normalization(s):
    normalized_s = normalize_terms_for_s(s)
    re_checked_s = checking_regular_expressions_for_s(normalized_s)

    return re_checked_s

# s = "Goutnil 0.5mg Tablet"
# s2 = "GOUTNIL 0.5MG TAB"
# rs = normalization(s)
# rs2 = normalization(s2)
# print(s, "---", rs)
# print(s2, "---", rs2)


